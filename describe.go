package main

import (
	"os"
	"fmt"
	"path/filepath"
)

// test string:  ~/.vimrc describe.go "hallo go world"

func main() {
	args := os.Args[1:]
	if len(args)  <=1 {
		fmt.Printf("Usage: describe <filename> <description>\n")
		return
	}

	hostname,_ := os.Hostname()
	fmt.Printf("Hostname: %s\n", hostname)
	fmt.Println(os.Getenv("JPSOFT_DESCR"))

	fmt.Printf("%v\n", args)
//	fmt.Println(args[2])

	desc := args[len(args)-1]
	for i := 0; i < len(args)-1; i++ {
		if fileExists(args[i]) {
			dir, file := filepath.Split(args[i])
			addDescription(dir, file, desc)
		}
	}
}

// fileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func fileExists(filename string) bool {
    _, err := os.Stat(filename)
    if os.IsNotExist(err) {
        return false
    }
    return  true
}

func addDescription(fileDir, fileName , description string) {
	fd := filepath.Join(fileDir,  os.Getenv("JPSOFT_DESCR"))
	_, err := os.Stat(fd)
	if os.IsNotExist(err) {
		file,_ := os.Create(fd)
		defer file.Close()
	}
	file,_ := os.OpenFile(fd, os.O_RDWR|os.O_APPEND, 0660)
	defer file.Close()
	fmt.Fprintf(file, "%s %s\n", fileName, description)

	fmt.Printf("add to %s\n", fd)
	fmt.Printf("describe %s %s\n", fileName, description)
}

